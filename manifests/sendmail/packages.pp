#Manifest ensures installation of sendmail for <= RHEL5
class coxauto-mail::sendmail::packages {
  
  package { 'sendmail':
    ensure => installed,
  }

}
