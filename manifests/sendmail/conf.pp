#Configuration definitions for sendmail

class coxauto-mail::sendmail::conf {

  file { '/etc/mail/sendmail.cf':
    content => template('coxauto-mail/sendmail.cf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }
}
