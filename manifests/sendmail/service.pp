#Service definitions for sendmail
class coxauto-mail::sendmail::service {

  service { 'sendmail':
    ensure => 'started',
  }

}
