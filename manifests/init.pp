#This module controls coxauto-mail settings - no params.pp;
#put that mess in hiera!
class coxauto-mail {

$OS = $::operatingsystem
$osrelease = $::operatingsystemmajrelease
$sendmailserver = hiera('sendmail_smart_relay')
$postfixserver = hiera('postfix_relayhost')
  
  if $OS == 'RedHat' {

    if $osrelease == '5' {
      include coxauto-mail::sendmail::packages
      include coxauto-mail::sendmail::service
      include coxauto-mail::sendmail::conf
    }

    elsif $osrelease >= '6' {
      include coxauto-mail::postfix::packages
      include coxauto-mail::postfix::service
      include coxauto-mail::postfix::conf
  }

    else {
      warning( 'The coxauto-mail configuration is not being applied! \
        It only support supports RedHat 5 and up systems,\
         please ensure you are running a supported OS.' )
    }
  }

}
