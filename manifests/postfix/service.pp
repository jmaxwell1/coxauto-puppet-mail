#Ensures postfix is running
class coxauto-mail::postfix::service {

  service { 'postfix':
    ensure => 'running',
    enable => true,
  }
}
