#Configuration definitions for postfix
#This class will only make changes if the system
#is not the postfix server.  This is because the 
#server settings are missing from the configuration
#template.  Hopefully, this is temporary
class coxauto-mail::postfix::conf {

  if $::hostname != $::postfixserver {
    
    file { '/etc/postfix/main.cf':
      content => template('coxauto-mail/main.cf.erb'),
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }
  }
}
