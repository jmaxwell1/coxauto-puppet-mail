# mail

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with mail](#setup)
    * [What mail affects](#what-mail-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with mail](#beginning-with-mail)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Configures parameters for postfix and sendmail MTA settings for both clients and
servers

## Module Description

This module ensures that postfix (greater than or equal to RHEL 6) or sendmail
(RHEL 5 or lower) is installed and configured.  It does contain server 
settings if the parameter in hiera $sendmailserver equals the hostname of the 
system.  If the hostname matches $postfix server it does not manage the 
configuration file.  This should be added at some point.

## Setup

No additional setup

### What mail affects

Packages, service, and configuration files related to postfix and sendmail


### Beginning with mail

The things are located within this module, the parameters are located in
Hiera


## Limitations

Module will error if it detects it is not running on RHEL five or greater

## Development

Water's fine!

## Changelog ##
2015.10.19 - Jamison - Added postfix settings

2015.10.16 - Jamison - Created structure and sendmail configuration template

2015.10.16 - Jamison - Module created and initial configurations have been written

